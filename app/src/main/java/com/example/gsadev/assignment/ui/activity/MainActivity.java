package com.example.gsadev.assignment.ui.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.gsadev.assignment.R;
import com.example.gsadev.assignment.utils.Utils;
import com.example.gsadev.assignment.data.ApiService;
import com.example.gsadev.assignment.data.dots.LessonData;
import com.example.gsadev.assignment.ui.adapter.CustomPagerAdapter;
import com.example.gsadev.assignment.view.CustomViewPager;
import com.example.gsadev.assignment.view.DepthPageTransformer;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.viewpager) CustomViewPager mPager;
    ApiService apiService;
    public LessonData lessonData;
    public int pagerIndex=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mPager.setPageTransformer(false,new DepthPageTransformer());
        apiService= Utils.getClientAPI().create(ApiService.class);
        Call<LessonData> call=apiService.getDateList();
        call.enqueue(new Callback<LessonData>() {
            @Override
            public void onResponse(Call<LessonData> call, Response<LessonData> response) {
                lessonData=((LessonData)response.body());
                mPager.setAdapter(new CustomPagerAdapter(getSupportFragmentManager(),
                        ((LessonData)response.body()).dataList.size()));
            }

            @Override
            public void onFailure(Call<LessonData> call, Throwable t) {
                Log.e(MainActivity.class.getName(),t.getMessage());
            }
        });


    }

    public int getPagerPosition(){
        return pagerIndex;
    }

   public void setPagerPosition(int position){
        position=pagerIndex+=1;
        mPager.setCurrentItem(position);
    }

}
