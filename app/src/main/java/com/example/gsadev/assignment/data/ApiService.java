package com.example.gsadev.assignment.data;


import com.example.gsadev.assignment.data.dots.LessonData;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by GSA Dev on 12/15/2017.
 */

public interface ApiService {
    @GET("getLessonData.php")
     Call<LessonData> getDateList();
}
