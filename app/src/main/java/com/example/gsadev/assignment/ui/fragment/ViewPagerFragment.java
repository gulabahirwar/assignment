package com.example.gsadev.assignment.ui.fragment;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gsadev.assignment.ui.activity.MainActivity;
import com.example.gsadev.assignment.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by GSA Dev on 12/15/2017.
 */

public class ViewPagerFragment extends Fragment{

    @BindView(R.id.title) TextView title;
    @BindView(R.id.sub_title) TextView subTitle;
    @BindView(R.id.textView)  TextView tips;
    @BindView(R.id.play_icon) ImageView iconPlay;
    @BindView(R.id.record_icon) ImageView recordVoice;

    MediaPlayer mediaPlayer;
    MainActivity activity;

    private Boolean isStarted = false;
    private Boolean isVisible = false;
    private static final int REQ_CODE_SPEECH = 100;
    int index;

    public static ViewPagerFragment newInstance(int num) {
        ViewPagerFragment f = new ViewPagerFragment();
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isStarted && isVisible) {
            playAudioByUrl();
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView =LayoutInflater.from(getContext()).inflate(R.layout.viewpager_item,container,false);
        index = getArguments() != null ? getArguments().getInt("num") : 1;
        ButterKnife.bind(this, rootView);
        activity=((MainActivity)getActivity());
        title.setText(activity.lessonData.dataList.get(index).pronunciation);
        subTitle.setText(activity.lessonData.dataList.get(index).conceptName);

        if (activity.lessonData.dataList.get(index).type.equals("learn")){
            tips.setText("tips to remind this");
            tips.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            tips.setTypeface(tips.getTypeface(), Typeface.NORMAL);
            tips.setTextColor(getResources().getColor(android.R.color.darker_gray));
            recordVoice.setImageResource(R.drawable.ic_light_bulb);
            recordVoice.setRotation(180);
            recordVoice.setClickable(false);
        }else {
            tips.setText("Hold to record");
            tips.setTextColor(getResources().getColor(android.R.color.holo_blue_bright));
            tips.setTypeface(tips.getTypeface(), Typeface.BOLD);
            recordVoice.setImageResource(R.drawable.ic_microphone);
            recordVoice.setRotation(0);
            recordVoice.setClickable(true);
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onPause() {
        super.onPause();
    }
    @OnClick(R.id.play_icon)
    public void playAudio(){
        if (mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            iconPlay.setImageResource(R.drawable.ic_play_button);
        }else{
            mediaPlayer.start();
            iconPlay.setImageResource(R.drawable.ic_music_player_pause_lines);
        }

    }

    @OnClick(R.id.image_next)
    public void nextPage(){
        mediaPlayer.stop();
        mediaPlayer.reset();
        //+1 for increasing page index
        activity.setPagerPosition(index);

    }

    @OnClick(R.id.record_icon)
    public void recordVoice(){
        Intent intent=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Speak now...");

        startActivityForResult(intent,REQ_CODE_SPEECH);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQ_CODE_SPEECH && data!=null){
            ArrayList<String> voiceText=data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

            if (voiceText.get(0).equalsIgnoreCase(title.getText().toString())){
                Toast.makeText(getContext(),"Matched",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getContext(),"Not Matched",Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible=isVisibleToUser;
        mediaPlayer=new MediaPlayer();
        if (isStarted && isVisible) {
            playAudioByUrl();
        }else{
            mediaPlayer.stop();
        }
    }

    private void playAudioByUrl() {
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(activity.lessonData.dataList.get(index).audio_url);
            mediaPlayer.prepare();

        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (!mp.isPlaying())
                iconPlay.setImageResource(R.drawable.ic_play_button);
            }
        });
        if (mediaPlayer.isPlaying()){
            iconPlay.setImageResource(R.drawable.ic_music_player_pause_lines);
        }

    }
    }

