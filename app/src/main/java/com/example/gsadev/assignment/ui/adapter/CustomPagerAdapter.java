package com.example.gsadev.assignment.ui.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.example.gsadev.assignment.ui.fragment.ViewPagerFragment;


/**
 * Created by GSA Dev on 12/15/2017.
 */

public class CustomPagerAdapter extends FragmentStatePagerAdapter {
   int count;
    public CustomPagerAdapter(FragmentManager fm,int count) {
        super(fm);
        this.count=count;
    }

    @Override
    public Fragment getItem(int position) {
        return ViewPagerFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return count;
    }

}
