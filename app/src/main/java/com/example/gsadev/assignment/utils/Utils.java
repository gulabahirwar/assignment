package com.example.gsadev.assignment.utils;

import com.example.gsadev.assignment.data.ApiService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by GSA Dev on 12/15/2017.
 */

public class Utils {
    private static ApiService sService = null;
    private static final String BASE_URL="http://www.akshaycrt2k.com/";
    static Retrofit retrofit=null;

    public static Retrofit getClientAPI(){
        if (retrofit==null){
         retrofit=new Retrofit.Builder()
                 .baseUrl(BASE_URL)
                 .addConverterFactory(GsonConverterFactory.create())
                 .build();
        }
        return retrofit;
    }

}
