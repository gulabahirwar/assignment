package com.example.gsadev.assignment.data.dots;

import com.google.gson.annotations.SerializedName;

/**
 * Created by GSA Dev on 12/14/2017.
 */

public class Data {
    @SerializedName("type")
    public String type;
    @SerializedName("conceptName")
    public String conceptName;
    @SerializedName("pronunciation")
    public String pronunciation;
    @SerializedName("targetScript")
    public String targetScript;
    @SerializedName("audio_url")
    public String audio_url;
}
