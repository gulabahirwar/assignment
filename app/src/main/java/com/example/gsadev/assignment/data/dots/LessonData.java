package com.example.gsadev.assignment.data.dots;

import com.example.gsadev.assignment.data.dots.Data;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by GSA Dev on 12/15/2017.
 */

public class LessonData {
    @SerializedName("lesson_data")
    public List<Data> dataList;

}
